'use strict';

let angular = require('angular');

import editor from './editor/editor.component';
import AppComponent from './app.component';

import FlowHistory from './common/flowHistory.service';
import FlowRunner from './common/flowRunner.service';
import FlowViewer from './flowViewer';


module.exports = angular.module('app', [
  // libs
  'ui.bootstrap',

  // us
  editor,
  FlowViewer
])
.constant('GOOG_KEY', 'AIzaSyCsGBI630NurtRoC-qkDOMbCyBFZs5h3HU')

.component('app', AppComponent)

.service('flowHistory', FlowHistory)
.service('flowRunner', FlowRunner)

// Some misc stuff that didn't fit anywhere else
.filter('limitToEllipsis', function () {
  return text => {
    if (text.length > 500) {
      return text.slice(0, 497).trim() + '...';
    } else {
      return text;
    }
  };
})
// Highlights the Google shortlink when the users saves
// http://stackoverflow.com/a/14996261/1216976
.directive('selectOnInit', ['$window', function ($window) {
  return {
    restrict: 'A',
    link: function (scope, element) {
      console.log('yey linky', $window.getSelection().toString());
      // Required for mobile Safari
      if (!$window.getSelection().toString()) {
        setTimeout(() =>
          element[0].setSelectionRange(0, element[0].value.length));
      }
    }
  };
}]);
