'use strict';

let outDir = 'prod';

let common = require('./wp-common')(outDir);
let webpack = require('webpack');

common.plugins.push(
  new webpack.optimize.UglifyJsPlugin({
    sourceMap: false,
    'screw-ie8': true,
    stats: true,
    comments: false,
    mangle: false
  })
);

module.exports = common;
