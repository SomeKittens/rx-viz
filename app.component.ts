import FlowRunner from './common/flowRunner.service';

class AppComponent {
  static $inject: string[];
  constructor(private flowRunner: FlowRunner) {}
}

AppComponent.$inject = ['flowRunner'];

export default {
  controller: AppComponent,
  template: require('./app.template.pug')
};
