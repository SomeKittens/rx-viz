import * as angular from 'angular';

import {IFlowStruct} from '../../common/flowRunner.service';

let sankey = require('../../sklibsankey');

class FlowRender {
  group: IFlowStruct;
  activeNode: number;
  isMain: boolean;
  flow: any;
  onData: any;
  handleClick: (any) => void;
  private static $inject = ['$scope', 'flowEmitter', 'config', '$element'];
  constructor(public $scope, public flowEmitter, public config, public $element) {
  }
  $postLink() {
    if (!this.isMain) {
      this.config = angular.copy(this.config);
      this.config = {
        node: {
          radius: 15,
          border: 1,
          // radius: 60,
          textPadding: 25
        },
        link: {
          width: 1
        },
        msgBall: {
          radius: 3,
          ttl: 2000
        },
        pointer: {
          radius: 3,
          length: 6,
          offset: -1
        },
        font: '0px sans-serif'
      };
      this.config.node.width = (this.config.node.radius + this.config.node.border) * 2;
    }

    // Declare all the variables!
    let canvas = this.$element[0].querySelector('canvas');
    // These are set later because right now canvas width/height is 0
    let width = canvas.width = canvas.clientWidth;
    let height = canvas.height = canvas.clientHeight;
    let context = canvas.getContext('2d');
    // let nodeNameToIdx;
    let diagramGraph;
    let msgs = [];
    let nodeColors = {
      highlighted: '#f92672',
      default: '#cc6633'
    };
    let getNodeById = id => diagramGraph.nodes.find(node => node.id === id);
    // Still haven't decided if each ball is the same or different
    // Alternatively: http://stackoverflow.com/a/16348977/1216976
    // let ballColors = [
    //   '#f92672',
    //   '#f4bf75',
    //   '#a6e22e',
    //   '#a1efe4',
    //   '#66d9ef',
    //   '#ae81ff'
    // ];
    let ballColors = ['#a6e22e'];
    let lightish = '#f8f8f2';

    // Calculate node positions
    // Ripped off of d3 Sankey stuff
    let recalcNodes = () => {
      // Remove one full node from calculated width
      // This is for padding along the edges
      let sWidth = width - (this.config.node.width + 20);

      diagramGraph = sankey({
        nodeWidth: this.config.node.width,
        nodePadding: 10,
        size: [sWidth, height],
        nodes: this.group.nodes,
        links: this.group.links,
      });

      let columns = diagramGraph.nodes.reduce((prev, node) => {
        if (!prev[node.x]) {
          prev[node.x] = [];
        }
        prev[node.x].push(node);
        return prev;
      }, {});

      Object.keys(columns).forEach(key => {
        let numInColumn = columns[key].length;
        let chunkSize = height / numInColumn;
        columns[key].forEach((node, i) => {
          node.y = (i * chunkSize) + (chunkSize / 2);
        });
      });

      diagramGraph.nodes.forEach(node => {
        // Sankey tries to make a bar, we have a fixed-height node in the middle
        // node.y = (node.y + node.dy) / 2;
        // Add padding so we don't clip off the edge
        node.x += this.config.node.width + 10;
      });
    };

    // Called every time the selected graph changes
    let init = () => {
      msgs = [];

      this.group.links.forEach(link => {
        link.value = 1;
      });

      recalcNodes();
    };
    this.onData = datum => {
      // In case we have added new observable data
      recalcNodes();

      // TODO: get all target nodes (instead of just +1)
      // TODO: object pooling
      let nodeId = datum.conf.source.id;
      let node = getNodeById(nodeId);

      // Datum may not be for our group
      if (!node) { return; }
      let color = ballColors[Math.floor(Math.random() * ballColors.length)];
      node.sourceLinks.forEach(ld => {
        let start = {
          x: ld.source.x + (this.config.msgBall.radius * 2),
          y: ld.source.y
        };
        let end = {
          x: ld.target.x - (this.config.msgBall.radius * 2),
          y: ld.target.y
        };
        let distance = {
          x: end.x - start.x,
          y: end.y - start.y
        };
        msgs.push({
          start: start,
          distance: distance,
          timestamp: Date.now(),
          color: color,
          source: datum.conf.source,
          target: datum.conf.target
        });
      });
    };
    this.flowEmitter.on('data', this.onData);

    this.$scope.$watch(() => this.group, init);

    let draw = () => {
      if (!diagramGraph || !canvas.clientWidth) {
        requestAnimationFrame(draw);
        return;
      }

      if (canvas.clientWidth !== width) {
        // Need to recalculate things based on new width
        width = canvas.width = canvas.clientWidth;
        height = canvas.height = canvas.clientHeight;

        recalcNodes();
      }

      context.fillStyle = '#272822';
      context.fillRect(0, 0, width, height);

      // Paint nodes
      diagramGraph.nodes.forEach((node) => {
        context.beginPath();
        switch (node.name) {
          case 'constructor':
            context.moveTo(node.x + this.config.node.radius, node.y);
            context.lineTo(node.x - this.config.node.radius, node.y - (this.config.node.radius));
            context.lineTo(node.x - this.config.node.radius, node.y + (this.config.node.radius));
            context.lineTo(node.x + this.config.node.radius, node.y);
          break;
          case 'subscribe':
            context.moveTo(node.x + this.config.node.radius, node.y + this.config.node.radius);
            context.lineTo(node.x + this.config.node.radius, node.y - this.config.node.radius);
            context.lineTo(node.x - this.config.node.radius, node.y - this.config.node.radius);
            context.lineTo(node.x - this.config.node.radius, node.y + this.config.node.radius);
          break;
          default:
            context.arc(node.x, node.y, this.config.node.radius, 0, 2 * Math.PI);
          break;
        }
        context.fillStyle = nodeColors.default;
        if (this.activeNode === node.id) {
          context.fillStyle = nodeColors.highlighted;
        }
        context.fill();

        // Label
        context.font = this.config.font;
        let computedName = `${node.name} (${node.id})`;
        var w = context.measureText(computedName).width;
        context.fillText(computedName, node.x - (w/2), node.y + this.config.node.radius + this.config.node.textPadding);
      });

      // Paint intra-node links
      diagramGraph.links.forEach(link => {
        context.strokeStyle = lightish;
        context.fillStyle = lightish;
        // Line across
        context.beginPath();
        context.moveTo(link.source.x + (this.config.node.radius + this.config.node.border), link.source.y);
        context.lineTo(link.target.x - (this.config.msgBall.radius), link.target.y);
        context.lineWidth = this.config.link.width;
        context.lineCap = 'round';
        context.stroke();

        // Pointy triangle
        context.beginPath();
        context.moveTo(link.target.x - this.config.msgBall.radius + this.config.pointer.offset, link.target.y);
        context.lineTo(link.target.x - this.config.pointer.length, link.target.y - this.config.pointer.radius);
        context.lineTo(link.target.x - this.config.pointer.length, link.target.y + this.config.pointer.radius);
        context.fill();
      });

      // Paint msg balls
      let now = Date.now();
      msgs.forEach(msg => {
        // Has it reached the target node?
        if (msg.timestamp + this.config.msgBall.ttl < now) {
          msg.done = true;
          return;
        }

        // How far along have we gone?
        let distanceTraveledX = Math.floor((now - msg.timestamp) / this.config.msgBall.ttl * msg.distance.x);
        let distanceTraveledY = Math.floor((now - msg.timestamp) / this.config.msgBall.ttl * msg.distance.y);

        context.beginPath();
        context.fillStyle = msg.color;
        context.arc(msg.start.x + distanceTraveledX, msg.start.y + distanceTraveledY, this.config.msgBall.radius, 0, 2 * Math.PI, false);
        context.fill();
      });

      requestAnimationFrame(draw);
    };

    // Start animating
    draw();

    function within(margin, x, y) {
      if (!x || !y) { return false; }
      return x + margin >= y && x - margin <= y;
    }

    // If the user clicks on a node, set that one to active
    this.handleClick = (e) => {
      if (!this.isMain) { return; }
      e.preventDefault();
      // http://stackoverflow.com/a/5932203/1216976
      let totalOffsetX = 0;
      let totalOffsetY = 0;
      let x = 0;
      let y = 0;
      let currentElement = e.target;

      do {
        totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
        totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
        currentElement = currentElement.offsetParent;
      } while(currentElement);

      x = e.pageX - totalOffsetX;
      y = e.pageY - totalOffsetY;

      diagramGraph.nodes.some(node => {
        if ((within(this.config.node.radius, x, node.x) && within(this.config.node.radius, y, node.y)) ||
          (within(100, x, node.x) && within(20, y, node.y + this.config.node.radius + this.config.node.textPadding))) {
          this.activeNode = node.id;
          return true;
        }
      });
    };
  }

  $onDestroy() {
    this.flowEmitter.removeListener('data', this.onData);
  }
}

export default {
  bindings: {
    group: '=',
    activeNode: '=',
    isMain: '='
  },
  template: '<canvas id="{{$ctrl.uniqueId}}" ng-click="$ctrl.handleClick($event)"></canvas>',
  controller: FlowRender
};
