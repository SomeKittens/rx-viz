import * as Rx from 'rxjs';

import FlowRunner from '../common/flowRunner.service';

class FlowViewer {
  activeNode: number;
  private static $inject = ['flowRunner', 'flowData', 'mockConsole'];
  constructor (private flowRunner: FlowRunner, private flowData, private mockConsole) {
  }
  $postLink() {
    this.activeNode = 0;
  }
}

export default {
  controller: FlowViewer,
  template: require('./flowViewer.template.pug')
}



// FlowList

// FlowRender
  // Trigger re-rendering of diagram
  // DC.active.name = Math.random().toString(36);
