import FlowRunner from '../../common/flowRunner.service';

class FlowList {
  private static $inject = ['flowRunner'];
  constructor(private flowRunner: FlowRunner) {}
}

export default {
  controller: FlowList,
  template: require('./flowList.template.pug')
};
