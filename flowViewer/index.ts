import * as angular from 'angular';

import FlowViewer from './flowViewer.component';
import FlowList from './flowList/flowList.component';
import FlowRender from './flowRender/flowRender.component';
import NodeDetails from './nodeDetails/nodeDetails.component';


export default angular
.module('flowView', ['ui.bootstrap'])

.component('flowViewer', FlowViewer)
.component('flowList', FlowList)
.component('flowRender', FlowRender)
.component('nodeDetails', NodeDetails)

.name;
