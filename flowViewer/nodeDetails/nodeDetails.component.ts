import {IFlowStruct, FlowRunner} from '../../common/flowRunner.service';
import FlowHistory from '../../common/flowHistory.service';

class NodeDetails {
  activeNode: number;
  private static $inject = ['$scope', 'flowHistory', 'flowRunner', 'mockConsole'];
  constructor(private $scope, private flowHistory: FlowHistory, private flowRunner: FlowRunner, private mockConsole) {
  }
  classByType (logType: string): string{
    switch (logType) {
      case 'log': return 'info';
      case 'warn': return 'warning';
      case 'error': return 'danger';
      default: return '';
    }
  }
}

export default {
  bindings: {
    activeNode: '='
  },
  controller: NodeDetails,
  template: require('./nodeDetails.template.pug')
};
// Deetz
// Terrible hacks because of @wesleycho
// tl;dr, whenever we update DC.active.nodes, the tabs change
// UI Bootstrap removes all the tabs, sets DC.activeId to the active tab
// With no active tabs, it's set to null
// Problem is, we still want this to be set
// So if we ever see it being set to null, just change it back
// https://github.com/angular-ui/bootstrap/issues/5656
// $scope.$watch('DC.activeId', (n, o) => {
//   if (n === null) { DC.activeId = o; }
// });
