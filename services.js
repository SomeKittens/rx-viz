'use strict';

let angular = require('angular');
let {flowData, myEmitter} = require('./terribleRx');
let app = angular.module('app');
let mockConsole = require('./mockConsole');

app
.service('flowData', function () { return flowData; })
.service('flowEmitter', function () { return myEmitter; })
.factory('mockConsole', ['$rootScope', function ($rootScope) {
  mockConsole.interceptor.__onLog = () => $rootScope.$evalAsync();
  return mockConsole;
}]);

app
.service('config', function () {
  this.node = {
    radius: 30,
    border: 1,
    textPadding: 25
  };

  this.link = {
    width: 4
  };

  this.msgBall = {
    radius: 10,
    ttl: 2000
  };

  this.font = '20px sans-serif';

  this.pointer = {
    radius: 10,
    length: 20,
    offset: 5
  };

  this.node.width = (this.node.radius + this.node.border) * 2;
})

.service('saveService', ['$http', 'GOOG_KEY', function ($http, GOOG_KEY) {
  this.syncStateToURL = code => window.location.hash = 'code=' + encodeURIComponent(code);

  this.saveState = code => {
    this.syncStateToURL(code);
    return $http.post(`https://www.googleapis.com/urlshortener/v1/url?key=${GOOG_KEY}`, {
      longUrl: window.location.href,
    })
    .then(res => res.data)
    .then(data => this.shortURL = data.id);
  };

  // $location annoys me
  let hash = window.location.hash.split('code=');
  if (hash.length === 1) { return; }

  this.hash = decodeURIComponent(hash[1]);
}]);
