'use strict';

/* This is terrible and you should never do this */

let Rx = require('rxjs');
let errStackParser = require('error-stack-parser');
const EventEmitter = require('events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();

let rxDebug = false;
let debugLog = (...args) => {
  if (rxDebug) {
    console.log(...args);
  }
};

/**
 * structure of flowData
 * {
 *   name: string,
 *   nodes: INode[],
 *   links: ILink[]
 * }
 *
 * INode {
 *   id: number,
 *   name: string,
 * }
 *
 * Both items in ILink point to a node's ID
 * ILink {
 *   target: number,
 *   source: number
 * }
 */

let reset = () => {
  flowData.nodes = [];
  flowData.links = [];
  id = -1;
  myEmitter.emit('reset');
};

let flowData = {
  name: 'testFlow',
  nodes: [],
  links: [],
  subs: [],
  reset
};

let registerNode = node => flowData.nodes.push(node);
let registerLink = (source, target) => flowData.links.push({source, target});

// Auto incrementing & zero based so need to initalize at zero
let id = -1;
// Do you take this function to be yours, through segfaults and OOM errors...
let iDo = (conf) => {
  debugLog('ido', conf);
  return (...args) => {
    debugLog('----------------');
    debugLog('intercepted data', ...args);
    debugLog('From node', conf.source.name, ', id:', conf.source.id);
    debugLog('to node', conf.dest.name, ', id:', conf.dest.id);

    // Need a timeout here so that we don't send data before the network is set up on the same tick
    setTimeout(() =>
      myEmitter.emit('data', {
        conf,
        args
      }), 0);
  };
};

let interceptLink = (source, target) => {
  registerLink(source.__instrumentId, target.__instrumentId);
  // Add our interceptor
  let nextInChain = source.__instrumentDo(iDo({
    source: {
      name: source.__nodeName,
      id: source.__instrumentId
    },
    dest: {
      name: target.__nodeName,
      id: target.__instrumentId
    }
  }));
  source.__childless = false;
  // Set details on __instrumentDo
  nextInChain.__instrumented = true;
  nextInChain.__nodeName = '__instrumentDo';
  return nextInChain;
};

let oldSubscribe = Rx.Observable.prototype.subscribe;
let oldDo = Rx.Observable.prototype.do;
let oldLift = Rx.Observable.prototype.lift;

let oldMergeAll = Rx.Observable.prototype.mergeAll;

Rx.Observable.prototype.subscribe = function (...args) {
  // `this` is the current node, ...args contains the thing we want to attach to us

  // We need this section to know when the last leg of a datums' journey has begun
  // But this is called a LOT, since every operation technically subscribes
  let thisObs;
  if (this.__instrumented) {
    // Ok, normal observable chain
    thisObs = this;
  } else if (this.source && this.source.__instrumented) {
    // We're a ConnectedObservable
    thisObs = this.source;
  } else if (this instanceof Rx.Subject) {
    // We're a subject
    // ¯\_(ツ)_/¯
    debugLog('subscribe called from Subject');
    return oldSubscribe.apply(this, args);
  } else if (this instanceof Rx.Observable) {
    // This is a constructor directly connected to a subscriber
    this.__instrumentId = ++id;
    this.__nodeName = 'constructor';

    // This essentially 'instruments' the constructor
    // We can't override Rx.Observable since things have already inherited from it
    debugLog('tagging constructor with fn', this._subscribe.toString());
    debugLog('tagging constructor from subscribe, id:', id);
    registerNode({
      name: this.__nodeName,
      id: this.__instrumentId
    });
    thisObs = this;
  } else {
    // PANIC
    // All I know is that if you're merging, you end up here
    // ^^ may be covered by above case
    debugLog('Panicking');
    return oldSubscribe.apply(this, args);
  }

  // No need to check our special nodes
  if (thisObs.__nodeName === '__instrumentDo') {
    return oldSubscribe.apply(this, args);
  }
  // thisObs is not a terminal node
  if (thisObs.__instrumented && !thisObs.__childless) {
    if ((args[0] instanceof Rx.Subscriber)) {
      // This is not a terminal subscriber
      // MapSubscriber, DoSubscriber, etc
      // In other words, it's already been lifted
      return oldSubscribe.apply(this, args);
    }
    // else, keep going to register a new subscriber
  }

  // This is what we do if we think this is a terminal subscriber
  let fnName = 'subscribe';

  thisObs.__childless = false; // Congratulations!
  thisObs.__instrumented = true;

  let nextId = ++id;
  // Add our interceptor
  let nextInChain = this.__instrumentDo(iDo({
    source: {
      name: thisObs.__nodeName,
      id: thisObs.__instrumentId
    },
    dest: {
      name: fnName,
      id: nextId
    }
  }));
  debugLog('tagging subscriber, id:', nextId, ...args);
  registerNode({
    name: fnName,
    id: nextId
  });

  debugLog('subscribing:', fnName, 'id', nextId, 'to', thisObs.__nodeName, 'id:', thisObs.__instrumentId);
  registerLink(thisObs.__instrumentId, nextId);

  let finalSubscription = oldSubscribe.apply(nextInChain, args);
  flowData.subs.push(finalSubscription);
  return finalSubscription;
};

// Allowing us to have our own .do function that doesn't get instrumented
// This way we can use .do for our instrumenting side effects without
// constantly instrumenting our own functions
Rx.Observable.prototype.__instrumentDo = function __instrumentDo(...args) {
  return oldDo.apply(this, args).delay(2000);
};

// .lift is called behind the scenes on every operator (I think)
// Override it so that we can attach .__instrumentDo on every fn call
Rx.Observable.prototype.lift = function liftWrapper(...args) {

  // Sadly, the best way to figure out what operator called us
  let e = new Error(`don't throw me`);
  let parsedStack = errStackParser.parse(e)
    // Normalize the frame names
    .map(stackFrame => (stackFrame.functionName || '')
      .replace(/.*Observable./, '')
      .replace(/.*prototype\./, ''));

  let fnName = parsedStack[1];
  let isInstrument = parsedStack[2] === '__instrumentDo';

  // Chrome
  if (fnName.includes('[as ')) {
    fnName = fnName.split(' ').pop().replace(']', '');
  }

  // Dirty FF hacks
  fnName = fnName.replace('_', '');

  if (!Rx.Observable.prototype[fnName]) {
    // Sometimes there are nodes that aren't on the prototype
    // Currently more trouble than it's worth ¯\_(ツ)_/¯
    // Stuff like merging messes this up
    // throw new Error('Could not find operator name, got: ' + fnName);
  }

  let nextInChain, nextId;

  if (isInstrument) {
    // This is lifting __instrumentDo
    // Give it the same id that the prev node has
    // This way we know what 'real' nodes are connecting to each other
    return oldLift.apply(this, args);
  }

  if (!this.__instrumented) {
    // We are lifting into a constructor for the first time
    this.__instrumented = true;
    this.__instrumentId = ++id;
    this.__nodeName = 'constructor';

    // This essentially 'instruments' the constructor
    // We can't override Rx.Observable since things have already inherited from it
    debugLog('tagging constructor, id:', id);
    let nodeConf = {
      name: this.__nodeName,
      id: this.__instrumentId
    };
    registerNode(nodeConf);
  }

  if (this.__childless) {
    this.__childless = false;
  }

  nextId = ++id;
  // Add our interceptor
  nextInChain = this.__instrumentDo(iDo({
    source: {
      name: this.__nodeName,
      id: this.__instrumentId
    },
    dest: {
      name: fnName,
      id: nextId
    }
  }));
  registerNode({
    name: fnName,
    id: nextId
  });
  // Set details on __instrumentDo
  nextInChain.__instrumented = true;
  nextInChain.__nodeName = '__instrumentDo';

  // Actually bring in the requested function
  nextInChain = oldLift.apply(nextInChain, args);

  // Tag the requested function
  nextInChain.__childless = true;
  nextInChain.__instrumentId = nextId;
  nextInChain.__instrumented = true;
  nextInChain.__nodeName = fnName;
  debugLog('lifting:', fnName, 'id', nextId, 'onto', this.__nodeName, 'id:', this.__instrumentId);
  registerLink(this.__instrumentId, nextId);
  // This is the actual node (not anything we've invented)
  return nextInChain;
};

// TODO: Implement concurrent & scheduler across the board

let instrumentedMerge = observables => {
  observables.forEach(obs => {
    if (!obs.__instrumented) {
      // We are lifting into a constructor for the first time
      obs.__instrumented = true;
      obs.__instrumentId = ++id;
      obs.__nodeName = 'constructor';

      // essentially 'instruments' the constructor
      // We can't override Rx.Observable since things have already inherited from it
      debugLog('tagging constructor, id:', id);
      let nodeConf = {
        name: obs.__nodeName,
        id: obs.__instrumentId
      };
      registerNode(nodeConf);
    }
  });

  // Create merge node
  let mergeNode = new Rx.Subject();
  mergeNode.__instrumented = true;
  mergeNode.__instrumentId = ++id;
  mergeNode.__nodeName = 'merge';
  registerNode({
    name: 'merge',
    id: mergeNode.__instrumentId
  });

  // Create connections between these observables and merge node
  observables.forEach(obs => {
    debugLog('merging id', obs.__instrumentId, 'into', mergeNode.__instrumentId);
    interceptLink(obs, mergeNode).subscribe(mergeNode);
  });

  return mergeNode;
};
// TODO: handle the rest of the args
Rx.Observable.prototype.merge = function protoMergeWrapper(otherObs) {
  return instrumentedMerge([this, otherObs]);
};

Rx.Observable.merge = function staticMergeWrapper(...observables) {
  console.log('static merge');
  return instrumentedMerge(observables);
};

Rx.Observable.prototype.mergeAll = function mergeAllWrapper(...args) {
  // TODO: determine if this needs to be instrumented
  debugLog('merging all', ...args);
  return oldMergeAll.apply(this, args);
};

module.exports = {
  flowData,
  myEmitter
};
