'use strict';

let {nest, d3ascending} = require('./mockd3');
let R = require('ramda');

// Yeah, yeah, a whole lib for one line.  You'll get over it
let sumLinkValue = R.compose(R.sum,R.map(R.pluck('value')));

// TODO: really, this should just take some config and return a POJO
// No need to keep current state, etc
module.exports = function({
  nodeWidth = 24,
  nodePadding = 8,
  size = [1, 1],
  nodes = [],
  links = [],
  numIterations = 32
 }) {
  // Make a deep copy for local use
  nodes = JSON.parse(JSON.stringify(nodes));
  links = JSON.parse(JSON.stringify(links));
  let sankey = {
    nodes, links
  };
  let getNodeById = id => nodes.find(node => node.id === id);

  // Populate the sourceLinks and targetLinks for each node.
  // Also, if the source and target are not objects, assume they are indices.
  function computeNodeLinks() {
    nodes.forEach(function(node) {
      node.sourceLinks = [];
      node.targetLinks = [];
    });
    links.forEach(function(link) {
      var source = link.source,
          target = link.target;
      if (typeof source === 'number') { source = link.source = getNodeById(link.source); }
      if (typeof target === 'number') { target = link.target = getNodeById(link.target); }
      source.sourceLinks.push(link);
      target.targetLinks.push(link);
    });
  }

  // Compute the value (size) of each node by summing the associated links.
  function computeNodeValues() {
    nodes.forEach(function(node) {
      node.value = Math.max(
        sumLinkValue(node.sourceLinks),
        sumLinkValue(node.targetLinks)
      );
    });
  }

  // Iteratively assign the breadth (x-position) for each node.
  // Nodes are assigned the maximum breadth of incoming neighbors plus one;
  // nodes with no incoming links are assigned breadth zero, while
  // nodes with no outgoing links are assigned the maximum breadth.
  function computeNodeBreadths() {
    var remainingNodes = nodes,
        nextNodes,
        x = 0;

    while (remainingNodes.length) {
      nextNodes = [];
      remainingNodes.forEach(function(node) {
        node.x = x;
        node.dx = nodeWidth;
        node.sourceLinks.forEach(function(link) {
          if (nextNodes.indexOf(link.target) < 0) {
            nextNodes.push(link.target);
          }
        });
      });
      remainingNodes = nextNodes;
      ++x;
    }

    moveSinksRight(x);
    scaleNodeBreadths((size[0] - nodeWidth) / (x - 1));
  }

  function moveSinksRight(x) {
    nodes.forEach(function(node) {
      if (!node.sourceLinks.length) {
        node.x = x - 1;
      }
    });
  }

  function scaleNodeBreadths(kx) {
    nodes.forEach(function(node) {
      node.x *= kx;
    });
  }

  function computeNodeDepths(iterations) {
    var nodesByBreadth = nest()
        .key(function(d) { return d.x; })
        .sortKeys(d3ascending)
        .entries(nodes)
        .map(function(d) { return d.values; });

    //
    initializeNodeDepth();
    resolveCollisions();
    for (var alpha = 1; iterations > 0; --iterations) {
      relaxRightToLeft(alpha *= 0.99);
      resolveCollisions();
      relaxLeftToRight(alpha);
      resolveCollisions();
    }

    function initializeNodeDepth() {
      let ky = Math.min(...nodesByBreadth.map(function(nodeCollection) {
        return (size[1] - (nodeCollection.length - 1) * nodePadding) / sumLinkValue(nodeCollection);
      }));
      if (!ky) { return; }

      nodesByBreadth.forEach(function(nodeCollection) {
        nodeCollection.forEach(function(node, i) {
          node.y = i;
          node.dy = node.value * ky;
        });
      });

      links.forEach(function(link) {
        link.dy = link.value * ky;
      });
    }

    function relaxLeftToRight(_alpha) {
      nodesByBreadth.forEach(function(nodeCollection) {
        nodeCollection.forEach(function(node) {
          if (node.targetLinks.length) {
            var y = sumLinkValue(node.targetLinks, weightedSource) / sumLinkValue(node.targetLinks);
            node.y += (y - center(node)) * _alpha;
          }
        });
      });

      function weightedSource(link) {
        return center(link.source) * link.value;
      }
    }

    function relaxRightToLeft(_alpha) {
      nodesByBreadth.slice().reverse().forEach(function(nodeCollection) {
        nodeCollection.forEach(function(node) {
          if (node.sourceLinks.length) {
            var y = sumLinkValue(node.sourceLinks, weightedTarget) / sumLinkValue(node.sourceLinks);
            node.y += (y - center(node)) * _alpha;
          }
        });
      });

      function weightedTarget(link) {
        return center(link.target) * link.value;
      }
    }

    function resolveCollisions() {
      nodesByBreadth.forEach(function(nodeCollection) {
        var node,
            dy,
            y0 = 0,
            n = nodeCollection.length,
            i;

        // Push any overlapping nodes down.
        nodeCollection.sort(ascendingDepth);
        for (i = 0; i < n; ++i) {
          node = nodeCollection[i];
          dy = y0 - node.y;
          if (dy > 0) { node.y += dy; }
          y0 = node.y + node.dy + nodePadding;
        }

        // If the bottommost node goes outside the bounds, push it back up.
        dy = y0 - nodePadding - size[1];
        if (dy > 0) {
          y0 = node.y -= dy;

          // Push any overlapping nodeCollection back up.
          for (i = n - 2; i >= 0; --i) {
            node = nodeCollection[i];
            dy = node.y + node.dy + nodePadding - y0;
            if (dy > 0) { node.y -= dy; }
            y0 = node.y;
          }
        }
      });
    }

    function ascendingDepth(a, b) {
      return a.y - b.y;
    }
  }

  function computeLinkDepths() {
    nodes.forEach(function(node) {
      node.sourceLinks.sort(ascendingTargetDepth);
      node.targetLinks.sort(ascendingSourceDepth);
    });
    nodes.forEach(function(node) {
      var sy = 0, ty = 0;
      node.sourceLinks.forEach(function(link) {
        link.sy = sy;
        sy += link.dy;
      });
      node.targetLinks.forEach(function(link) {
        link.ty = ty;
        ty += link.dy;
      });
    });

    function ascendingSourceDepth(a, b) {
      return a.source.y - b.source.y;
    }

    function ascendingTargetDepth(a, b) {
      return a.target.y - b.target.y;
    }
  }

  function center(node) {
    return node.y + node.dy / 2;
  }

  computeNodeLinks();
  computeNodeValues();
  computeNodeBreadths();
  computeNodeDepths(numIterations);
  computeLinkDepths();
  return sankey;
};
