'use strict';

let outDir = 'test';

let common = require('./wp-common')(outDir);
let path = require('path');

common.devServer = {
  outputPath: path.join(__dirname, 'dist'),
  historyApiFallback: {
    rewrites: [
      {from: /\./, to: '/'}
    ]
  }
};
common.debug = true;
common.devtool = 'inline-source-map';

if (process.env.NODE_ENV === 'local') {
  let Dashboard = require('webpack-dashboard');
  let DashboardPlugin = require('webpack-dashboard/plugin');
  let dashboard = new Dashboard();

  common.plugins.push(new DashboardPlugin(dashboard.setData));
  common.devServer.quiet = true;
}


module.exports = common;
