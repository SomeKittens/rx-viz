# RxFiddle

Somehow you've managed to find this codebase hidden away in GitLab.  Congrats!

This is a project to visualize RxJS 5 Observable flows.  Pictures say a thousand words, moving pictures more than that.

All of this documentation comes with no guarantee of anything.  It's in GitLab so folks don't think I'm supporting this as a full thing, it's just a side project that I work on when I feel like it.

Typical setup:

 - Clone
 - `npm install`
 - `npm start`

If any of this breaks, let me know [via Twitter](https://twitter.com/rkoutnik) (I'm not monitoring the project's issues.)
