'use strict';

let logHistory = [];
let interceptor = {};

Object.keys(console)
.filter(key => typeof console[key] === 'function')
.forEach(key => {
  interceptor[key] = (...args) => {
    logHistory.push({
      key,
      args: args
    });
    console[key](...args);
    if (interceptor.__onLog) {
      interceptor.__onLog();
    }
  };
});

module.exports = {
  logHistory,
  interceptor,
  clear: () => logHistory.length = 0
};
