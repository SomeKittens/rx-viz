'use strict';

import * as angular from 'angular';
import * as CodeMirror from 'codemirror';

let obsString = 'let sourceObs = new Rx.Observable(o => {\n' +
'  [1,2,3].forEach((item, idx) =>\n' +
'    setTimeout(() => o.next(item), idx * 1000));\n' +
'})';
// Presets
let presets = {
  sync: '\n.map(x => x * 2)\n.map(x => Math.pow(x, 2))\n.filter(x => x > 6)\n.subscribe(console.log);',
  async: `\n.map(x => x + Math.floor(Math.random() * 150))\n.mergeMap(x => fetch('https://pokeapi.co/api/v2/pokemon/' + x)\n  .then(res => res.json()))\n.map(pokeJSON => pokeJSON.name)\n.subscribe(console.log);`,

  multiFlow: `;\nlet double = sourceObs.map(x => x * 2);\nlet square = sourceObs.map(x => x * x);\ndouble.merge(square)\n.subscribe(console.log);`,
  multiSub: ';\nlet double = sourceObs.map(x => x * 2);\nlet square = sourceObs.map(x => x * x);\ndouble.subscribe(console.log);\nsquare.subscribe(console.log);',
  parallel: '\n.subscribe(console.log);\nRx.Observable.interval(750).take(3).map(x => 5).subscribe(console.log);',
  nto1: `;\nlet obs2 = Rx.Observable.interval(750).take(3);\nRx.Observable.merge(sourceObs, obs2)\n.subscribe(console.log);`,
  nton: ';\nlet obs2 = Rx.Observable.interval(750).take(3);\nlet merged$ = Rx.Observable.merge(sourceObs, obs2);\nmerged$.subscribe(console.log);\nmerged$.subscribe(console.log);'
};

class EditorComponent {
  flow: string;
  editor: CodeMirror.Editor;
  urlSyncDebounce: () => void;
  static $inject = ['saveService', '$timeout', 'flowRunner']
  constructor(private saveService, private $timeout, private flowRunner) {
    let timeout;

    this.urlSyncDebounce = () => {
      clearTimeout(timeout);
      timeout = setTimeout(() => this.saveService.syncStateToURL(this.flow), 2000);
    };
  }
  loadPreset (option) {
    this.flow = obsString + presets[option];
    if (this.editor) {
      this.editor.getDoc().setValue(this.flow);
      setTimeout(() => {
        this.editor.refresh();
      });
    }
    this.flowRunner.runFlow(this.flow);
  }
  $onInit() {
    if (!this.saveService.hash) {
      this.loadPreset('sync');
    } else {
      this.flow = this.saveService.hash;
    }
  }
  $postLink() {
    // For some reason this needs to run on the next tick
    this.$timeout(() => {
      let textarea = <HTMLTextAreaElement>document.querySelector('.rx-input-operators');

      this.editor = CodeMirror.fromTextArea(textarea, {
        mode: 'javascript',
        lineWrapping: true,
        lineNumbers: true,
        theme: 'monokai'
      });

      this.editor.on('change', mirrorInstance => {
        this.flow = mirrorInstance.getValue();
        this.urlSyncDebounce();
        this.saveService.shortURL = '';
      });
      // Actually trigger everything
      this.flowRunner.runFlow(this.flow);
    });
  }
}

module.exports = angular
.module('editor', [])
.component('editor', {
  bindings: {
    parseError: '<'
  },
  template: require('./editor.template.pug'),
  controller: EditorComponent,
  controllerAs: 'EC'
})
.name;
