'use strict';

require('./terribleRx');
let Rx = require('rxjs');
let Observable = Rx.Observable;

let parallel = () => {
  Rx.Observable.of(1,2,3, Rx.Scheduler.async)
  .map(x => x * x)
  .subscribe(x => console.log('subscription 1 got', x));
  Rx.Observable.of(1,2,3, Rx.Scheduler.async)
  .map(x => x * x)
  .subscribe(x => console.log('subscription 2 got', x));
};

let noOperators = () => {
  Rx.Observable.of(1,2,3, Rx.Scheduler.async)
  .subscribe(x => console.log('subscription 1 got', x));
};

let nton = () => {
  let pubObs = new Observable(o => {
    [1,2,3].forEach((item, idx) =>
      setTimeout(() => o.next(item), idx * 1000));
  });
  let obs2 = Observable.interval(1000);
  let merge = Observable.merge(pubObs, obs2);
  merge.subscribe(x => console.log('sub 1 got' + x));
  merge.subscribe(x => console.log('sub 2 got' + x));
};

let manySubscribers = () => {
  let pubObs = new Rx.Observable(o => {
    [1,2,3].forEach((item, idx) =>
      setTimeout(() => o.next(item), idx * 1000));
  }, Rx.Scheduler.async);
  pubObs.subscribe(x => console.info('sub 1 got', x));
  pubObs.subscribe(x => console.info('sub 7 got', x));
  pubObs.subscribe(x => console.info('sub bring got', x));
  pubObs.map(() => 'tuna').subscribe(x => console.info('sub sandwich got', x));
  let subscription = pubObs.subscribe(x => console.info('sub 2 got', x));
  return {pubObs, subscription};
};

manySubscribers();
// myObs
// .filter(x => x % 2)
// .do(x => console.log('normal dooooo, got', x))
// .subscribe(x => console.log('subscription got', x));

// myObs
// .map(x => x / 2)
// .filter(x => x === Math.abs(x))
// .subscribe(x => console.log('other sub got', x));
