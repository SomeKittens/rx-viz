import * as Rx from 'rxjs';

import FlowHistory from './flowHistory.service';

export interface IFlowStruct {
  nodes: IFlowNode[];
  links: IFlowLink[];
}

export interface IFlowNode {
  name: string;
  id: number;
  groupId: number;
}

export interface IFlowLink {
  source: number;
  target: number;
  value?: number;
}

export class FlowRunner {
  subscription: any;
  parseError: string;
  groups: IFlowStruct[];
  activeGroup: IFlowStruct;
  private static $inject = ['flowData', 'flowEmitter', 'mockConsole', 'flowHistory'];
  private flow: string;
  constructor(private flowData, private flowEmitter, private mockConsole, private flowHistory: FlowHistory) {
    this.flowEmitter.on('data', () => {
      let needsUpdate = this.flowData.nodes.some(node => node.groupId === undefined);
      if (needsUpdate) {
        this.parseRawFlow(this.flowData);
      }
    });
  }

  runFlow(flow: string) {
    console.clear();
    if (flow) {
      this.flow = flow;
    } else {
      console.log('should this even happen?');
    }

    // Reset the things
    this.flowData.reset();
    if (this.subscription && this.subscription.unsubscribe) {
      this.subscription.unsubscribe();
    }
    while (this.flowData.subs.length) {
      this.flowData.subs.pop().unsubscribe();
    }
    this.mockConsole.clear();
    this.flowHistory.clear();
    this.parseError = '';

    // Generate the Rx flow (Rx is instrumented at this point)
    let flowFn;
    try {
      /* jshint ignore:start */
      flowFn = new Function('Rx', 'console', `
        with (Rx) {
          ${this.flow}
        }
        //# sourceURL=fnParser.js
      `);
      /* jshint ignore:end */
    } catch (e) {
      console.log(e);
      this.parseError = e;
      return;
    }

    let subscription;
    try {
      subscription = flowFn(Rx, this.mockConsole.interceptor);
    } catch (e) {
      // TODO: Better var name
      this.parseError = e;
      return;
    }
    this.subscription = subscription;
    this.parseRawFlow(this.flowData);
  }

  private parseRawFlow (flow) {
    // given flowdata

    // start with first node
    // Give it a groupId
    // give all links same group id
    let nextGroupId = 0;
    this.groups = [];
    let getNodeById = id => flow.nodes.find(node => node.id === id);
    let getLinksByNodeId = id => flow.links.filter(link => link.source === id || link.target === id);

    flow.nodes
    // Quick hack
    .map(node => {
      delete node.groupId;
      return node;
    })
    .forEach(node => {

      if (node.groupId === undefined) {
        // Tag this node with a new group
        node.groupId = nextGroupId;
        nextGroupId++;
      }
      if (!this.groups[node.groupId]) {
        this.groups[node.groupId] = {
          nodes: [],
          links: []
        };
      }
      this.groups[node.groupId].nodes.push(node);

      // Spread this group id to all connected nodes
      // Awful big-0 but simple enough in practice
      getLinksByNodeId(node.id).forEach(link => {
        if (!this.groups[node.groupId].links.includes(link)) {
          this.groups[node.groupId].links.push(link);
        }
        if (link.source === node.id) {
          getNodeById(link.target).groupId = node.groupId;
        } else if (link.target === node.id) {
          getNodeById(link.source).groupId = node.groupId;
        }
      });
    });

    this.activeGroup = this.groups[0];
  }
}

export default FlowRunner;
