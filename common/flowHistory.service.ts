export default class FlowHistory {
  nodeEmitted: any[];
  nodeReceived: any[];
  public static $inject: string[];
  constructor(private $rootScope, private flowEmitter) {
    this.nodeEmitted = [];
    this.nodeReceived = [];

    flowEmitter.on('data', data => {
      if (!this.nodeEmitted[data.conf.source.id]) {
        this.nodeEmitted[data.conf.source.id] = [];
      }
      this.nodeEmitted[data.conf.source.id].push(data);

      if (!this.nodeReceived[data.conf.dest.id]) {
        this.nodeReceived[data.conf.dest.id] = [];
      }
      this.nodeReceived[data.conf.dest.id].push(data);

      $rootScope.$evalAsync();
    });
  }
  clear() {
    this.nodeEmitted = [];
    this.nodeReceived = [];
  }
}

FlowHistory.$inject = ['$rootScope', 'flowEmitter'];
