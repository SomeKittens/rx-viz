'use strict';

require('codemirror/lib/codemirror.css');
require('codemirror/theme/monokai.css');

require('./sass/entry.scss');

require('angular');
require('angular-ui-bootstrap');

require('./sklibsankey');

require('./ng.js');
require('./services.js');
require('./terribleRx');
