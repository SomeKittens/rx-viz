'use strict';

process.env.UV_THREADPOOL_SIZE = 100;

let path = require('path');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let CopyWebpackPlugin = require('copy-webpack-plugin');
let webpack = require('webpack');
let execSync = require('child_process').execSync;

let git = {
  sha: execSync('git rev-parse HEAD').toString(),
  date: execSync('git show -s --format=%ci HEAD').toString(),
  version: require('./package.json').version
};

let filesToCopy = [
  'index.html',
  'test.html',
  'img/rx_logo.png',
  // Favicon stuff
  'img/android-chrome-192x192.png',
  'img/android-chrome-512x512.png',
  'img/apple-touch-icon.png',
  'img/browserconfig.xml',
  'img/favicon-16x16.png',
  'img/favicon-32x32.png',
  'img/favicon.ico',
  'img/index.html',
  'img/manifest.json',
  'img/mstile-150x150.png',
  'img/safari-pinned-tab.svg'
];

let loaders = [
  {
    test: /\.(png|jpg)$/,
    loader: 'url-loader',
    query: { mimetype: 'image/png' }
  },
  {
    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    loader: 'url-loader?limit=10000&minetype=application/font-woff'
  },
  {
    test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    loader: 'file-loader'
  },
  {
    test: /\.s[ac]ss$/,
    loader: ExtractTextPlugin.extract('style', 'css?sourceMap!sass')
  },
  {
    test: /\.css$/,
    loader: ExtractTextPlugin.extract('style', 'css?sourceMap')
  },
  {
    test: /\.js$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'babel?presets[]=es2015'
  },
  {
    test: /\.json$/,
    loader: 'json-loader'
  },
  { test: /\.ts$/, loader: 'awesome-typescript-loader' },
  {
    test: /\.(pug|jade)$/,
    loader: 'pug-html-loader'
  }
];

module.exports = outDir => {
  return {
    entry: {
      app: './entry.js',
      vendor: ['angular', 'angular-ui-bootstrap', 'ramda', 'rxjs', 'events', 'error-stack-parser',
      'codemirror/mode/javascript/javascript', 'codemirror/lib/codemirror']
    },
    output: {
      filename: 'bundle.js',
      path: path.join(__dirname, 'dist')
    },
    module: {
      loaders: loaders
    },
    resolve: {
      modulesDirectories: [
        'node_modules',
        'bower_components'
      ],
      extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },
    plugins: [
      new ExtractTextPlugin('./[name].css'),
      new CopyWebpackPlugin(filesToCopy.map(filename => ({
        from: filename,
        to: './' + filename
      }))),
      new webpack.DefinePlugin({
        GIT_SHA: JSON.stringify(git.sha.trim()),
        GIT_DATE: JSON.stringify(git.date.trim()),
        VERSION: JSON.stringify(git.version.trim()),
        ENV: JSON.stringify(process.env.NODE_ENV || outDir)
      }),
      new webpack.optimize.CommonsChunkPlugin('vendor', './vendor.bundle.js')
    ]
  };
};
